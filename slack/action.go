package slack

import "github.com/nlopes/slack"

type Action func(*ActionData)

type ActionData struct {
	slack.MessageEvent
	Bot    *Bot
	Params map[string]string
}

func (a *ActionData) Reply(r string) error {
	_, _, err := a.Bot.RTMAPI.PostMessage(a.MessageEvent.Channel, r, slack.PostMessageParameters{AsUser: true})

	return err
}

func (a *ActionData) Log(s ...string) {
	a.Bot.Log(s...)
}

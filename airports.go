package main

import (
	"io/ioutil"
	"strings"

	"github.com/gocarina/gocsv"
)

type Airport struct {
	Name      string  `csv:"Airport Name"`
	City      string  `csv:"City"`
	Country   string  `csv:"Country"`
	IATA      string  `csv:"IATA"`
	ICAO      string  `csv:"ICAO"`
	Longitude float64 `csv:"Longitude`
	Latitude  float64 `csv:"Latitude"`
	Altitude  int     `csv:"Altitude"`
}

type Airports []Airport

func LoadAirports(af string) (Airports, error) {
	var airports Airports
	data, err := ioutil.ReadFile(af)
	if err != nil {
		return nil, err
	}

	if err = gocsv.UnmarshalBytes(data, &airports); err != nil {
		return nil, err
	}

	return airports, nil
}

func (a Airports) Lookup(q string) *Airport {
	for _, ap := range a {
		q = strings.ToLower(q)
		if q == strings.ToLower(ap.IATA) || q == strings.ToLower(ap.ICAO) {
			return &ap
		}
	}

	return nil
}

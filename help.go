package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/davidjpeacock/shelbot-ii/slack"
)

type Quote struct {
	QuoteText   string `json:"quoteText"`
	QuoteAuthor string `json:"quoteAuthor"`
	SenderName  string `json:"senderName"`
	SenderLink  string `json:"senderLink"`
	QuoteLink   string `json:"quoteLink"`
}

func getQuote(a *slack.ActionData) {
	var q Quote

	resp, err := http.Get("http://api.forismatic.com/api/1.0/?format=json&method=getQuote&lang=en")
	if err != nil || resp.StatusCode != 200 {
		a.Reply(fmt.Sprintf("<@%s> When I lost my own father, I didn't have any friends to help me through it. You do.", a.User))
		return
	}
	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&q); err != nil {
		a.Reply(fmt.Sprintf("<@%s> When I lost my own father, I didn't have any friends to help me through it. You do.", a.User))
		return
	}
	a.Reply(fmt.Sprintf("<@%s>\n>%s\n_%s_", a.User, q.QuoteText, q.QuoteAuthor))
}
